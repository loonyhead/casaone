import React from 'react';
import { connect } from "react-redux";
import {
  onDeleteItem,
  onAddItem,
  onSave,
  onDeliveryDateChange,
  onAddressChange,
  onChangeItem,
  onHideToast,
  onCloseItemPicker,
  onOpenItemPicker,
  onSelectItem,
} from './actions';
import './App.css';
import AddressBox from './components/AddressBox';
import DateBox from './components/DateBox';
import CartDetails from './components/CartDetails';
import Button from './components/Button';
import Toast from './components/Toast';
import ItemPicker from './components/ItemPicker';

const AddressConst = {
  firstNanme: "First Name",
  lastName: "Last Name",
  address1: "Address Line 1",
  address2: "Address Line 2",
  city: "City",
  state: "State",
  zip: "Zipcode",
  country: "Country",
}
const froratAddress = (data) => {
  return Object.keys(data).map((item)=>{
    return {
      name: item,
      value: data[item],
      displayName: AddressConst[item],
    }
  });
};

const App = ({
  billingAddress,
  shippingAddress, 
  orderDate, 
  deliveryDate, 
  cart,
  showToast,
  toastMsg,
  onDeleteItem,
  onAddItem,
  onDeliveryDateChange,
  onAddressChange,
  onChangeItem,
  onHideToast,
  showItemPicker,
  products,
  onCloseItemPicker,
  onOpenItemPicker,
  onSelectItem,
}) => {
  return (
    <div className="App">
      {showToast && 
        (<Toast data={toastMsg} close={onHideToast} />)
      }
      {showItemPicker &&
        (<ItemPicker data={products} onClose={onCloseItemPicker} selectItem={onSelectItem} />)
      }
      <div className="billingPanel row">
        <div className="leftPanel">
          <AddressBox title="Billing Address" data={froratAddress(billingAddress)} onChange={(e)=>onAddressChange(e, "billingAddress")} />
          <DateBox title="Order Date" value={orderDate} name="orderDate" disabled />
        </div>
        <div className="rightPanel">
          <AddressBox title="Shiping Address" data={froratAddress(shippingAddress)} onChange={(e)=>onAddressChange(e, "shippingAddress")} />
          <DateBox title="Delivery Date" value={deliveryDate} name="deliveryDate" handleChange={onDeliveryDateChange} />
        </div>
      </div>
      <div className="cartPanel">
        <CartDetails data={cart} deleteItem={onDeleteItem} addItem={onAddItem} onChange={onChangeItem} onSelect={onOpenItemPicker} />
        <div className="cartAction">
          <Button color="blue" label="Save" extClass="update" handleClick={onSave} />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
    billingAddress: state.billingAddress,
    shippingAddress: state.shippingAddress,
    orderDate: state.orderDate,
    deliveryDate: state.deliveryDate,
    cart: state.cart,
    showToast: state.showToast,
    toastMsg: state.toastMsg,
    showItemPicker: state.showItemPicker,
    products: state.products,
});

export default connect(
  mapStateToProps, 
  {
    onDeleteItem,
    onAddItem,
    onSave,
    onDeliveryDateChange,
    onAddressChange,
    onChangeItem,
    onHideToast,
    onCloseItemPicker,
    onOpenItemPicker,
    onSelectItem,
  }
)(App);