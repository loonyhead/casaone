import { store } from './reducer';
import * as types from './constants';

const addressError = (type, name) => {
    const types = {
        billingAddress: 'Billing Address',
        shippingAddress: 'Shipping Address',
    };
    const fields = {
        firstNanme: "First Name",
        lastName: "Last Name",
        address1: "Address Line 1",
        address2: "Address Line 2",
        city: "City",
        state: "State",
        zip: "Zipcode",
        country: "Country",
    };
    return `${fields[name]} cannot be empty in ${types[type]}`;
};

export const onDeleteItem = (id) => ({
    type: types.DELETE_PRODUCT,
    payload: id,
});

export const onAddItem = () => ({
    type: types.ADD_PRODUCT,
});

export const onDeliveryDateChange = (e) => {
    let orderDate = store.getState().orderDate;
    let newDate = e.target.value;
    let dateDiff = (new Date(newDate) - new Date(orderDate))/(1000*3600*24);
    if(dateDiff< 3) 
        return ({
            type: types.SHOW_TOAST,
            payload: {
                type: "error",
                msg: "Delivery Date should be atleast 3 days after the ordered date"
            },
        });
    else return ({
            type: types.DELIVERY_DATE_CHANGE,
            payload: newDate,
        });
};

export const onAddressChange = (e, type) => {
    const { name, value } = e.target; 
    if(value === '') 
        return {
            type: types.SHOW_TOAST,
            payload: {
                type: "error",
                msg: addressError(type, name),
            },
        }
    else return {
            type: types.ADDRESS_CHANGE,
            payload: {
                type,
                name,
                value,
            },
    };
};

export const onChangeItem = (id, e ) => {
    const { name, value } = e.target;   
    if(name === "quantity" && value < 1) {
        return {
            type: types.SHOW_TOAST,
            payload: {
                type: "error",
                msg: "Quantity should be more than 1",
            },
        }
    }  
    return ({
        type: types.ON_CHANGE_ITEM,
        payload: {
            id,
            name,
            value,
        },
    }); 
};

export const onHideToast = () => ({
    type: types.HIDE_TOAST,
});
export const onOpenItemPicker = (id) =>({
    type: types.SHOW_ITEM_PICKER,
    payload: id,
});
export const onCloseItemPicker = () => ({
    type: types.HIDE_ITEM_PICKER,
});
export const onSelectItem = (item) => {
return ({
    type: types.ITEM_SELECTED,
    payload: item,
});
};
export const onSave = () => {
    const {cart} = store.getState();
    console.log("============= Cart Details =============")
    console.log(cart);
}