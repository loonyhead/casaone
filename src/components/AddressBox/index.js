import React from 'react';
import InputDir from '../InputDir';

const AddressBox = (props) => {
    const { title, data, onChange } = props;
    return (
      <div>
        <h4>{title}</h4>
        <div>
            {data && data.map((item)=>(
                <InputDir type="text" value={item.value} name={item.name} displaName={item.displaName} handleChange={onChange} />
            ))}
        </div>
      </div>
    );
  };
  
  
  export default AddressBox;