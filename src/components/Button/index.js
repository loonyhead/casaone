import React from 'react';

const Button = (props) => {
    const { label, type, handleClick, color, extClass } = props;
    if(type==="select")
        return (
        <button onClick={handleClick} className={color}>&#926;</button>
        );
    else
        return (
            <button onClick={handleClick} className={extClass ? `${color} ${extClass}` : color}>{label}</button>
        );
  };
  
  
  export default Button;