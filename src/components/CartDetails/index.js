import React from 'react';
import CartItem from '../CartItem';
import Button from '../Button';

const CartDetails = (props) => {
    const { data, deleteItem, addItem, onChange, onSelect } = props;
    if(data.length < 1)
    return (
        <>
            <div>Cart is Empty</div>
            <Button label="Add Product" color="blue" extClass="addItem" handleClick={addItem} />
        </>
    );
    return (
        <div> 
            <div className="cartHeader desktopOnly row">
                <div className="fifteen cItem">Product ID</div>
                <div className="thirty cItem">Product Name</div>
                <div className="ten cItem">Quantity</div>
                <div className="ten cItem">Unit Price</div>
                <div className="ten cItem">Total Price</div>
                <div className="fifteen cItem">Notes</div>
                <div className="ten cItem">&nbsp;</div>
            </div>
            {data.map((item)=>(
                <CartItem data={item} deleteItem={deleteItem} onChange={onChange} onSelect={onSelect} />
            ))}
            <Button label="Add Product" color="blue" extClass="addItem" handleClick={addItem} />
        </div>
    );
  };
  
  
  export default CartDetails;