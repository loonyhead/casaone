import React from 'react';
import InputDir from '../InputDir';
import Button from '../Button';

const CartItem = (props) => {
    const { data, deleteItem, onChange, onSelect } = props;
    const { quantity, itemId, id, name, price } = data;
    const total = quantity*price;
    const onChangeHandler = (e) => {
        onChange(data.itemId, e);
    };
    return (
        <div className="cartItem row"> 
            <div className="fifteen cItem">
                <div className="mobileOnly">Product ID</div>
                <InputDir disabled type="text" showSelection={id===null} value={id} name="id" onSelect={()=>onSelect(itemId)} />
            </div>
            <div className="thirty cItem">
                <div className="mobileOnly">Product Name</div>
                <InputDir disabled type="text" value={name} name="name" />
            </div>
            <div className="ten cItem">
                <div className="mobileOnly">Quantity</div>
                <InputDir type="number" value={quantity} name="quantity" handleChange={onChangeHandler} />  
            </div>
            <div className="ten cItem">
                <div className="mobileOnly">Unit Price</div>
                <InputDir disabled type="number" value={price} name="price" />  
            </div>
            <div className="ten cItem">
                <div className="mobileOnly">Total Price</div>
                <InputDir type="number" value={total} name="total" disabled />  
            </div>
            <div className="fifteen cItem">
                <div className="mobileOnly">Notes</div>
                <InputDir type="textArea" name="notes" handleChange={onChangeHandler} />  
            </div>
            <div className="ten cItem">
                <Button color="red" label="Delete" handleClick={()=>deleteItem(data.itemId)} />
            </div>
        </div>
    );
  };
  
  
  export default CartItem;