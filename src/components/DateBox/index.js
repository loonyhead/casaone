import React from 'react';
import InputDir from '../InputDir';

const DateBox = (props) => {
    const { title, value, name, handleChange, disabled=false } = props;
    return (
      <div className="dateBox">
        <h4>{title}</h4>
        <InputDir handleChange={handleChange} type="date" disabled={disabled} value={value} name={name} displayName="Order Date" extClass="dateIco" />
      </div>
    );
  };
  
  
  export default DateBox;