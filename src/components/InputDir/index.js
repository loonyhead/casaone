import React from 'react';
import Button from '../Button';

const InputDir = (props) => {
    const {type, value, name, displayName, handleChange, showSelection, onSelect, extClass, disabled=false} = props;
    if(type==="textArea")
        return (
            <div>
                <textarea disabled={disabled} onChange={handleChange} className="inputBox" rows="2" name={name} placeholder={displayName}>{value}</textarea>
            </div>
        );
    else
        return (
            <div>
                {!showSelection && (<input disabled={disabled} onChange={handleChange} className={extClass ? `inputBox ${extClass}` : "inputBox"} type={type} name={name} value={value} placeholder={displayName} />)}
                {showSelection && (
                    <div className="btnSelection">
                        <input disabled={disabled} onChange={handleChange} className="inputBox" type={type} name={name} value={value} placeholder={displayName} />
                        <Button type="select" handleClick={onSelect} color="blue" />
                    </div>
                )}
            </div>
        );  
};


export default InputDir;
