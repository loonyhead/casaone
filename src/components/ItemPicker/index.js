import React from 'react';

const ItemPicker = (props) => {
    const { data, selectItem, onClose } = props;
        return (
            <>
                <div className="layer" onClick={onClose} />
                <div className="pickItem">
                    <h2>Select Item</h2>
                    <ul>
                        {data.map((item)=>(<li onClick={()=>selectItem(item)}>{item.name}</li>))}
                    </ul>
                </div>
            </>
        );
  };
  
  
  export default ItemPicker;