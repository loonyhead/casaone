import { createStore } from 'redux';
import uuid from 'uuid/v4';
import * as types from './constants';

const initialState = {
    orderId: "12345",
    orderDate: "2019-10-02",
    deliveryDate: "2019-10-06",
    billingAddress: {
        firstNanme: "John",
        lastName: "Doe",
        address1: "34 Oak Street",
        address2: "Hartford Avenue",
        city: "Houston",
        state: "Texas",
        zip: "AZX23R",
        country: "USA",
    },
    shippingAddress: {
        firstNanme: "John",
        lastName: "Doe",
        address1: "34 Oak Street",
        address2: "Hartford Avenue",
        city: "Houston",
        state: "Texas",
        zip: "AZX23R",
        country: "USA",
    },
    cart: [
        {
            itemId: uuid(),
            id: 10000,
            name: "Stockholm Dining Table",
            price: 529,
            quantity: 8,
            notes: '',
        },
        {
            itemId: uuid(),
            id: 10041,
            name: "Oulu Dining Chair Tangerine (Set of 4 Units)",
            price: 105,
            quantity: 2,
            notes: '',
        },
        {
            itemId: uuid(),
            id: 10042,
            name: "Oulu Dining Chair Graphite (Set of 4 Units)",
            price: 105, 
            quantity: 12,
            notes: '',
        },
    ],
    products: [
        {
            id: 10000,
            name: "Stockholm Dining Table",
            price: 529,
        },
        {
            id: 10041,
            name: "Oulu Dining Chair Tangerine (Set of 4 Units)",
            price: 105,
        },
        {
            id: 10042,
            name: "Oulu Dining Chair Graphite (Set of 4 Units)",
            price: 105,
        },
        {
            id: 10045,
            name: "Oulu Dining Chair Graphite 1",
            price: 120,
        },
        {
            id: 10046,
            name: "Oulu Dining Chair Graphite 2",
            price: 150,
        },
        {
            id: 10047,
            name: "Oulu Dining Chair Graphite 3",
            price: 180,
        }
    ],
    showToast: false,
    toastMsg: null,
    showItemPicker: false,
    itemPickedFor: null,
};

const reducer = (state, {type, payload}) => {
    switch(type){
        case types.DELETE_PRODUCT: {
            const cart = state.cart.filter((item) => item.itemId !== payload);
            return {
                ...state,
                cart,
            };
        };
        case types.ADD_PRODUCT: 
            return {
                ...state,
                cart: [...state.cart, {
                    itemId: uuid(),
                    id: null,
                    quantity: 1,
                    price: 0,
                    notes: '',
                }]
            };
        case types.DELIVERY_DATE_CHANGE:
            return {
                ...state,
                deliveryDate: payload,
            }
        case types.ADDRESS_CHANGE:
            return {
                ...state,
                [payload.type] : {
                    ...state[payload.type],
                    [payload.name]: payload.value,
                }
            };
        case types.ON_CHANGE_ITEM: {
            const cart = state.cart.map((item) => {
                if(item.itemId === payload.id)
                    return {
                        ...item,
                        [payload.name]: payload.value,
                    }
                else 
                    return item;
            });
            return {
                ...state,
                cart,
            }
        }
        case types.SHOW_TOAST:
            return {...state, showToast: true, toastMsg: payload};
        case types.HIDE_TOAST:
            return {...state, showToast: false, toastMsg: null};
        case types.SHOW_ITEM_PICKER: 
            return {...state, showItemPicker: true, itemPickedFor: payload};
        case types.HIDE_ITEM_PICKER: 
            return {...state, showItemPicker: false, itemPickedFor: null};
        case types.ITEM_SELECTED: {
            const cart = state.cart.map((item) => {
                if(item.itemId === state.itemPickedFor)
                    return {
                        ...item,
                        id: payload.id,
                        price: payload.price,
                        name: payload.name,
                    }
                else 
                    return item;
            });
            return {
                ...state,
                cart,
                showItemPicker: false,
                itemPickedFor: null,
            };
        }
        default:
            return state;
    }
};

export const store = createStore(
    reducer,
    initialState,
    window.devToolsExtension && window.devToolsExtension()
);
